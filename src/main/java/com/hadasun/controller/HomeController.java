package com.hadasun.controller;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hadasun.model.Product;
import com.hadasun.model.ProductDTO;
import com.hadasun.service.impl.ProductService;

@Controller
public class HomeController {

	@Inject
	private ProductService productService;
	
	@RequestMapping({"","/"})
	public String home() {
		return "redirect:/products";
	}

	@RequestMapping("/products")
	public String showHome(Model model) {
		List<Product> products = productService.getAllProducts();
		model.addAttribute("products", products);
		return "home";
	}

	@RequestMapping("/product")
	public String getProductById(@RequestParam("id") String productId,
			Model model) {
		model.addAttribute("product", productService.findByProductId(productId));
		return "product";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String getAddNewProductForm(Model model) {
		ProductDTO newProductDTO = new ProductDTO();
		model.addAttribute("newProductDTO", newProductDTO);
		return "addProduct";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAddNewProductForm(
			@ModelAttribute("newProductDTO") @Valid ProductDTO newProductDTO, BindingResult result, HttpServletRequest request) {
		if (result.hasErrors()) {
			return "addProduct";
		}
		MultipartFile productImage = newProductDTO.getProductImage();
		String rootDirectory = request.getSession().getServletContext()
				.getRealPath("/");
		if (productImage != null && !productImage.isEmpty()) {
			try {
				productImage.transferTo(new File(rootDirectory
						+ "resources\\images\\" + newProductDTO.getProductId()
						+ ".jpg"));
			} catch (Exception e) {
				throw new RuntimeException("Product Image saving failed", e);
			}
		}
		Product newProduct = new Product();
		newProduct.setCategory(newProductDTO.getCategory());
		newProduct.setDescription(newProductDTO.getDescription());
		newProduct.setManufacturer(newProductDTO.getManufacturer());
		newProduct.setName(newProductDTO.getName());
		newProduct.setProductId(newProductDTO.getProductId());
		newProduct.setUnitPrice(newProductDTO.getUnitPrice());
		newProduct.setUnitsInOrder(newProductDTO.getUnitsInOrder());
		newProduct.setUnitsInStock(newProductDTO.getUnitsInStock());
		productService.save(newProduct);
		return "redirect:/products";
	}

	@RequestMapping("/{category}")
	public String getAllProducts(Model model,
			@PathVariable("category") String productCategory) {
		List<Product> products = productService
				.getProductByCategory(productCategory);
		model.addAttribute("products", products);
		return "home";
	}

	@RequestMapping("/nuochoa")
	public String showNuocHoapage() {
		return "nuochoa";
	}

	/*@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(Model model, @RequestParam String username,
			@RequestParam String password) {
		User user = userService.findByUsernameAndPassword(username, password);
		if (user != null) {
			List<Product> products = productService.getAllProducts();
			model.addAttribute("products", products);
			return "home";
		} else {
			return "login";
		}
	}*/

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String loginerror(Model model) {
		model.addAttribute("error", "true");
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/denied")
 	public String denied() {
		return "denied";
	}
}