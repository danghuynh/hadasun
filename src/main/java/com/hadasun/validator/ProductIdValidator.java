package com.hadasun.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hadasun.model.Product;
import com.hadasun.service.impl.ProductService;

@Component
public class ProductIdValidator implements
		ConstraintValidator<ProductId, String> {
	@Autowired
	private ProductService productService;

	public void initialize(ProductId constraintAnnotation) {
	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		Product product;
		product = productService.findByProductId(value);
		if (product != null) {
			return false;
		}
		return true;
	}
}