package com.hadasun.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.hadasun.model.Order;
import com.hadasun.repository.api.OrderRepository;
import com.hadasun.service.api.CartService;

public class OrderServiceImpl {
	
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private CartService cartService;
	
	public Long saveOrder(Order order) {
		Long orderId = orderRepository.saveOrder(order);
		cartService.delete(order.getCart().getCartId());
		return orderId;
		}

}
