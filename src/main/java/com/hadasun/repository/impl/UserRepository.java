package com.hadasun.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hadasun.model.User;

/*@Repository
 public interface UserRepository extends JpaRepository<User, Integer>{

 User findByUsername(String username);

 }
 */
@Repository
public class UserRepository {

	@Autowired
	private SessionFactory sessionFactory;

	public User findByUsername(String username) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		List<User> users = (List<User>) criteria.list();
		tx.commit();
		session.close();
		if (users != null && users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}
	
	public User findByUsernameAndPassword(String username, String password) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		criteria.add(Restrictions.eq("password", password));
		List<User> users = (List<User>) criteria.list();
		tx.commit();
		session.close();
		if (users != null && users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

}