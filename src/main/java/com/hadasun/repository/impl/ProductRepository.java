package com.hadasun.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hadasun.model.Product;

@Repository
public class ProductRepository {

	@Autowired
	private SessionFactory sessionFactory;

	public Product findByName(String name) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Product.class);
		criteria.add(Restrictions.eq("name", name));
		List<Product> products = (List<Product>) criteria.list();
		tx.commit();
		session.close();
		if (products != null && products.size() > 0) {
			return products.get(0);
		} else {
			return null;
		}
	}
	
	public Product findByProductId(String productId) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Product.class);
		criteria.add(Restrictions.eq("productId", productId));
		List<Product> products = (List<Product>) criteria.list();
		tx.commit();
		session.close();
		if (products != null && products.size() > 0) {
			return products.get(0);
		} else {
			return null;
		}
	}

	public void save(Product product) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(product);
		tx.commit();
		session.close();
	}

	public List<Product> getAllProducts() {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Product.class);
		List<Product> products = (List<Product>) criteria.list();
		tx.commit();
		session.close();
		return products;
	}
	
	public List<Product> getProductByCategory(String category) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Product.class);
		criteria.add(Restrictions.eq("category", category));
		List<Product> products = (List<Product>) criteria.list();
		tx.commit();
		session.close();
		return products;
	}

}
