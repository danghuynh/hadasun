package com.hadasun.repository.api;

import com.hadasun.model.Order;

public interface OrderRepository {
	Long saveOrder(Order order);
}
