package com.hadasun.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hadasun.model.Role;

@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {

	@Inject
	private UserService userService;

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		com.hadasun.model.User user = userService.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(
					"User cannot be found with username: " + username);
		}

		return new User(user.getUsername(), user.getPassword(),
				buildUserAuthority(user.getRole()));
	}

	private List<GrantedAuthority> buildUserAuthority(Role userRole) {
		System.out.println("add role to user");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		int role = userRole.getRole();
		if (role == 1) {
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		} else if (role == 2) {
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		}
		return authorities;
	}

}
