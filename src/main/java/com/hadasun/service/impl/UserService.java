package com.hadasun.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hadasun.model.User;
import com.hadasun.repository.impl.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User findByUsername(String username) {
		return  userRepository.findByUsername(username);
	}
	
	public User findByUsernameAndPassword(String username, String password) {
		return  userRepository.findByUsernameAndPassword(username, password);
	}

}
