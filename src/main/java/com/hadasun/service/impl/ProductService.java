package com.hadasun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hadasun.model.Product;
import com.hadasun.repository.impl.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public Product findByName(String name) {
		return  productRepository.findByName(name);
	}
	
	public Product findByProductId(String productId) {
		return  productRepository.findByProductId(productId);
	}
	
	public void save(Product product) {
		productRepository.save(product);
	}
	
	public List<Product> getAllProducts() {
		return productRepository.getAllProducts();
	}
	
	public List<Product> getProductByCategory(String category) {
		return productRepository.getProductByCategory(category);
	}

}
