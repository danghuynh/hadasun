<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:insertDefinition name="tcareness">
	<tiles:putAttribute name="body">
	<link rel="stylesheet" href="resources/css/gridview.css">
		<div class="container">
		    <div id="products" class="row list-group">
		        <div class="item list-group-item col-xs-4 col-lg-4">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="http://static.phongtro123.com/uploads/2016/05/1-26-225x300.jpg" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        PHÒNG TIỆN NGHI TRUNG TÂM QUẬN 1, SÁT BỜ KÈ MÁT MẺ, GIÁ 3TR</h4>
		                    <p class="group inner list-group-item-text">
		                        Phòng cho thuê đường Nguyễn Đình Chiểu, Q1 ngay Bờ Kè.Nhà tọa lạc ngay khu quận 1 sầm uất, gần bờ kè Hoàng Sa mát mẻ, tiện làm việc ngay trung tâm Q1,…</p>
		                    <div class="row">
		                        <div class="col-xs-12 col-md-6">
		                            <p class="lead">
		                                Giá: <strong>3 Triệu/tháng</strong></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="item list-group-item col-xs-4 col-lg-4">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="http://static.phongtro123.com/uploads/2016/03/12742180_1709196245992900_4641861932464866068_n-300x225.jpg" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        Nhà trọ cao cấp giá bình dân Quận 7, Thủ Đức, Tân Phú, TP.HCM</h4>
		                    <p class="group inner list-group-item-text">
		                        Nhà trọ cao cấp giá bình dân - giờ giấc tự do - Địa chỉ 1: 113 Nguyễn Hậu, Q Tân Phú  - Địa chỉ 2: 11/15 đường số 10, Lý Phục Man, Q.7 - Địa chỉ 3:…</p>
		                    <div class="row">
		                        <div class="col-xs-12 col-md-6">
		                            <p class="lead">
		                                Giá: <strong>3 Triệu/tháng</strong></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="item list-group-item col-xs-4 col-lg-4">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="http://static.phongtro123.com/uploads/2015/11/phongtroquan7.11-300x169.jpg" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        Cho Thuê phòng có M.Lạnh, gần ĐH Maketing &#8211; Luật &#8211; NT Thành, KCX Tân Thuận, Huỳnh Tấn Phát, Q.7 &#8211; 3,2Tr/th</h4>
		                    <p class="group inner list-group-item-text">
		                        CHO THUÊ PHÒNG TRỌ QUẬN 7 HẺM 156 HUỲNH TẤN PHÁT GẦN KHU CHẾ XUẤT TÂN THUẬN, CÁC TRƯỜNG ĐẠI HỌC MAKETING, ĐẠI HỌC NGUYỄN TẤT THÀNH, ĐẠI HỌC LUẬT…</p>
		                    <div class="row">
		                        <div class="col-xs-12 col-md-6">
		                            <p class="lead">
		                                Giá: <strong>3 Triệu/tháng</strong></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>