<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<nav class="navbar navbar-fixed-top navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home"></span> Trang chủ</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tìm phòng trọ<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Hồ Chí Minh</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Hà Nội</a></li>
					</ul>
				</li>
				<li><a href="#">Đăng tin</a></li>
			</ul>
			<form class="navbar-form navbar-right">
				<button type="submit" class="btn btn-success">Đăng nhập</button>
				<button type="submit" class="btn btn-success">Đăng ký</button>
			</form>
			<form class="navbar-form navbar-right">
            	<input type="text" class="form-control" placeholder="Tìm kiếm...">
            	<button type="submit" class="glyphicon glyphicon-search" style="padding:8px 12px; top: 3px;"></button>
          	</form>
		</div>
	</div>
</nav>
	<!--MAIN MENU JAVASRIPT-->
<script>
$(document).ready(function(){


    $("#csDa").mouseenter(function(){
        $("#skinCare").css("visibility", "visible");
		$("#skinCare").css("box-shadow"," -4px 2px 2px black");
    });
	$("#csDa").mouseleave(function(){
        $("#skinCare").css("visibility", "hidden");
    });
	
	
	
	$("#nuocHoa").mouseenter(function(){
        $("#Fragnance").css("visibility", "visible");
		$("#Fragnance").css("box-shadow"," -4px 2px 2px black");
    });
	$("#nuocHoa").mouseleave(function(){
        $("#Fragnance").css("visibility", "hidden");
    });
	
	
	$("#thuongHieu").mouseenter(function(){
        $("#Brand").css("visibility", "visible");
		$("#Brand").css("box-shadow"," -4px 2px 2px black");
    });
	$("#thuongHieu").mouseleave(function(){
        $("#Brand").css("visibility", "hidden");
    });
	
});
</script>

<!--SALE EVENT JAVASCRIPT-->
<script>
var imagecount=1;
var total=2;
function slide(x){
	var Image = document.getElementById('saleImage');
	imagecount = imagecount + x;
	if (imagecount>total)
	{
		imagecount=1;
	}
	if (imagecount<1)
	{
		imagecount=total;
	}
	Image.src = "resources/image/img" + imagecount + ".jpg";
}
window.setInterval(function slideA(){
	var Image = document.getElementById('saleImage');
	imagecount = imagecount + 1;
	if (imagecount>total)
	{
		imagecount=1;
	}
	if (imagecount<1)
	{
		imagecount=total;
	}
	Image.src = "resources/image/img" + imagecount + ".jpg";
},5000)
</script>
	
<div class="pagecontent">
	<div id="seImage">
		<a href="#"><img id="saleImage" src="resources/image/img1.jpg" alt="Sale Event"></a>
	</div>