<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<tiles:insertDefinition name="tcareness">
<tiles:putAttribute name="body">
	<div class="property-list">
<ul class="pro-list">
   <li>
   <div class="p-vip">
   <div class="e-code">275880</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-an-ninh-gio-giac-tu-do-trung-tam-binh-thanh-6113" title="Phòng trọ an ninh, giờ giấc tự do, trung tâm Binh Thạnh">Phòng trọ an ninh, giờ giấc tự do, trung tâm Binh Thạnh</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 274/1 Bùi Đình Túy, phường 12, Bình Thạnh</div>
    <div class="e-area">Diện tích: <span>16</span>m²  |  Giá thuê: <span> 2.300.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">14/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">385146</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-o-duong-bach-dang-quan-tan-binh-trong-san-bay-tan-son-nhat-6112" title="Phòng Trọ ở đường Bạch Đằng, quận Tân Bình trong sân bay Tân Sơn Nhất">Phòng Trọ ở đường Bạch Đằng, quận Tân Bình trong sân bay ...</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 139 Bạch Đằng, Phường 2, quận Tân Bình</div>
    <div class="e-area">Diện tích: <span>20</span>m²  |  Giá thuê: <span> 2.300.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">14/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">356997</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/can-cho-thue-phong-trong-can-ho-gia-tu-3-tr-thang-lien-he-0974414415-6105" title="Cần cho thuê phòng trong căn hộ giá từ 3 tr/tháng. Liên hệ: 0974414415">Cần cho thuê phòng trong căn hộ giá từ 3 tr/tháng. Liên hệ: ...</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: Chung cư Era Town, quận 7, tpHCM</div>
    <div class="e-area">Diện tích: <span>40</span>m²  |  Giá thuê: <span> 3.000.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">13/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">802586</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-cao-cap-trong-can-ho-hoang-anh-gold-house-5966" title="PHÒNG CAO CẤP TRONG CĂN HỘ HOÀNG ANH GOLD HOUSE">PHÒNG CAO CẤP TRONG CĂN HỘ HOÀNG ANH GOLD HOUSE</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: NGUYỄN HỮU THỌ, QUÂN 7</div>
    <div class="e-area">Diện tích: <span>25</span>m²  |  Giá thuê: <span> 3.000.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">12/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">897364</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-cho-thue-sach-dep-day-du-tien-nghi-phu-nhuan-6107" title="Phòng cho thuê sạch đẹp, đầy đủ tiện nghi Phú Nhuận">Phòng cho thuê sạch đẹp, đầy đủ tiện nghi Phú Nhuận</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 94C2 Phùng Văn Cung, Phường 7, Quận Phú Nhuận, Thành phố Hồ ...</div>
    <div class="e-area">Diện tích: <span>30</span>m²  |  Giá thuê: <span> 5.000.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">12/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">569932</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-phong-261-263-nguyen-hong-dao-p-14-qtb-5986" title="cho thuê phòng 261. - 263 Nguyễn Hồng Đào P 14 QTB">cho thuê phòng 261. - 263 Nguyễn Hồng Đào P 14 QTB</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 261-263 Nguyễn Hồng Đào P14 QTB</div>
    <div class="e-area">Diện tích: <span>16</span>m²  |  Giá thuê: <span> 1.500.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">11/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">268092</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-phong-tro-nam-ngay-trung-tam-quan-3-6106" title="Cho Thuê Phòng Trọ Nằm Ngay Trung Tâm Quận 3">Cho Thuê Phòng Trọ Nằm Ngay Trung Tâm Quận 3</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 178 Võ Thị Sáu phường 7 quận 3</div>
    <div class="e-area">Diện tích: <span>18</span>m²  |  Giá thuê: <span> 3.300.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">10/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">634327</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-n-thue-phong-tro-gia-1-3-trieu-57-8a-duong-3-p-tang-nhon-phu-b-quan-9-tphcm-6104" title="Cho nữ thuê phòng trọ giá 1.3 triệu, 57/8A Đường 3, P.Tăng Nhơn Phú B, Quận 9, TPHCM">Cho nữ thuê phòng trọ giá 1.3 triệu, 57/8A Đường 3, P.Tăng Nhơn ...</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 57/8A Đường 3, P.Tăng Nhơn Phú B, Q.9, TP.HCM</div>
    <div class="e-area">Diện tích: <span>14</span>m²  |  Giá thuê: <span> 1.300.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">09/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">920948</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-nguyen-tang-moi-sach-se-gio-giac-tu-do-6103" title="cho thuê nguyên tầng mới sạch sẽ, giờ giấc tự do">cho thuê nguyên tầng mới sạch sẽ, giờ giấc tự do</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 95/24 phan văn trị,q2,q5</div>
    <div class="e-area">Diện tích: <span>25</span>m²  |  Giá thuê: <span> 3.000.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">07/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">235773</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/q-4-duong-xom-chieu-cho-thue-nguyen-phong-hoac-nguyen-lau-on-dinh-lau-dai-1tr250-nguoi-6101" title="Q.4, đường XÓM CHIẾU, cho thuê nguyên phòng hoặc nguyên lầu ổn định, lâu dài: 1tr250/người.">Q.4, đường XÓM CHIẾU, cho thuê nguyên phòng hoặc nguyên lầu ổn ...</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: QUẬN 4, P.15, đường XÓM CHIẾU- gần cầu KÊNH TẺ, TP.HCM</div>
    <div class="e-area">Diện tích: <span>28</span>m²  |  Giá thuê: <span> 1.250.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">06/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">665637</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-nam-cong-nhan-vien-sinh-vien-thue-phong-tro-6037" title="cho nam công nhân viên, sinh viên thuê phòng trọ">cho nam công nhân viên, sinh viên thuê phòng trọ</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 708/1/4 Hong Bang , P1, Q11</div>
    <div class="e-area">Diện tích: <span>15</span>m²  |  Giá thuê: <span> 1.500.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">06/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">017872</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-phong-moi-xay-477-le-hong-phong-p2-q10-6061" title="CHO THUÊ PHÒNG MỚI XÂY 477 LÊ HỒNG PHONG P2 Q10">CHO THUÊ PHÒNG MỚI XÂY 477 LÊ HỒNG PHONG P2 Q10</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 477 LÊ HỒNG PHONG P2 Q10</div>
    <div class="e-area">Diện tích: <span>20</span>m²  |  Giá thuê: <span> 2.800.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">06/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">015300</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-moi-xay-903-duong-3-2-p7-q11-gia-tu-1tr8-den-2tr2-6100" title="PHÒNG TRỌ MỚI XÂY 903 ĐƯỜNG 3/2 P7 Q11 GIÁ TỪ 1TR8 ĐẾN 2TR2">PHÒNG TRỌ MỚI XÂY 903 ĐƯỜNG 3/2 P7 Q11 GIÁ TỪ 1TR8 ĐẾN 2TR2</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 903 ĐƯỞNG 3/2 P7 Q11</div>
    <div class="e-area">Diện tích: <span>15</span>m²  |  Giá thuê: <span> 2.000.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">06/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">326482</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-can-ho-nho-gan-san-bay-tsn-6066" title="Cho thuê căn hộ nhỏ gần sân bay TSN">Cho thuê căn hộ nhỏ gần sân bay TSN</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: thân nhân trung, hcm</div>
    <div class="e-area">Diện tích: <span>25</span>m²  |  Giá thuê: <span> 2.500.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">05/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">826738</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/ha-noi/cho-thue-phong-gia-re-6098" title="cho thuê phòng giá rẻ">cho thuê phòng giá rẻ</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: số 1 ngách 5/16 ngõ 5 đường Tân Triều, phố Triều Khúc, ...</div>
    <div class="e-area">Diện tích: <span>12</span>m²  |  Giá thuê: <span> 1.000.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/ha-noi" title="Khu vực Hà Nội">Hà Nội</a></div>
    <div class="e-public">05/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">819090</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-gia-re-doi-dien-toa-nha-etown-cong-hoa-6005" title="Phòng trọ giá rẻ ( Đối diện tòa nhà Etown Cộng Hòa )">Phòng trọ giá rẻ ( Đối diện tòa nhà Etown Cộng Hòa )</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 03 Thân Nhân Trung - F.12 - Q.Tân Bình</div>
    <div class="e-area">Diện tích: <span>20</span>m²  |  Giá thuê: <span> 1.800.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">05/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">953833</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-cao-cap-moi-xay-duong-lam-van-ben-6069" title="Phòng trọ cao cấp mới xây đường Lâm Văn Bền ">Phòng trọ cao cấp mới xây đường Lâm Văn Bền</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: số 11 đường số 35 Lâm Văn Bền, phường Bình Thuận, Quận ...</div>
    <div class="e-area">Diện tích: <span>18</span>m²  |  Giá thuê: <span> 2.200.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">04/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">965660</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-sinh-thai-chinh-chu-quan-12-duong-phan-van-hon-6096" title="Phòng trọ sinh thái chính chủ Quận 12 đường Phan Văn Hớn">Phòng trọ sinh thái chính chủ Quận 12 đường Phan Văn Hớn</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: hem 167 duong phan van hon kp5 phuong tan thoi nhat q12</div>
    <div class="e-area">Diện tích: <span>14</span>m²  |  Giá thuê: <span> 1.300.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">03/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">809465</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-phong-tro-6095" title="cho thuê phòng trọ">cho thuê phòng trọ</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 28/20 Đường 16, Linh Chiểu, Thủ Đức</div>
    <div class="e-area">Diện tích: <span>24</span>m²  |  Giá thuê: <span> 2.200.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">03/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">680317</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-phong-day-du-tien-nghi-6094" title="cho thuê phòng đầy đủ tiện nghi">cho thuê phòng đầy đủ tiện nghi</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 666/38 đường 3/2-P14-Quận 10_TPHCM</div>
    <div class="e-area">Diện tích: <span>30</span>m²  |  Giá thuê: <span>       3₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">02/05/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">482777</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-n-thue-phong-tro-duong-pham-the-hien-quan-8-6093" title="Cho nữ thuê phòng trọ, đường Phạm Thế Hiển, quận 8">Cho nữ thuê phòng trọ, đường Phạm Thế Hiển, quận 8</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: Hẻm 1225 Phạm Thế Hiển p5 q8</div>
    <div class="e-area">Diện tích: <span>18</span>m²  |  Giá thuê: <span> 2.400.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">30/04/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">316408</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-gia-re-quan-1-cach-ben-thanh-1km-6092" title="Phòng trọ giá rẻ quận 1 cách Bến Thành 1km">Phòng trọ giá rẻ quận 1 cách Bến Thành 1km</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 391/144 Trần Hưng Đạo, Cầu Kho, Quận 1, Hồ Chí Minh</div>
    <div class="e-area">Diện tích: <span>15</span>m²  |  Giá thuê: <span>       2₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">29/04/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">116211</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/nha-tro-gia-re-quan-7-cach-lotte-mart-1km-6091" title="Nhà trọ giá rẻ quận 7 cách Lotte Mart 1km">Nhà trọ giá rẻ quận 7 cách Lotte Mart 1km</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 1041/62/59 Trần Xuân Soạn, Tân Hưng, Quận 7, Hồ Chí Minh</div>
    <div class="e-area">Diện tích: <span>32</span>m²  |  Giá thuê: <span> 2.700.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">29/04/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">628040</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/phong-tro-gia-re-q10-cao-thang-6083" title="Phòng trọ giá rè Q10 - Cao Thắng">Phòng trọ giá rè Q10 - Cao Thắng</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 232/27 Cao Thắng - q.10 - TPHCM</div>
    <div class="e-area">Diện tích: <span>18</span>m²  |  Giá thuê: <span>       1₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">29/04/2016</div>
    </div>
   </li>
    <li>
   <div class="p-vip">
   <div class="e-code">054974</div>
  
   </div>
   <div class="p-left">

    <div class="e-title">
     
               <h3><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh/cho-thue-nguyen-lau-6090" title="CHO THUÊ NGUYÊN LẦU">CHO THUÊ NGUYÊN LẦU</a></h3>
                

    </div>
    <div class="e-address">Địa chỉ: 101/6 A BẮC HẢI P15 Q10</div>
    <div class="e-area">Diện tích: <span>40</span>m²  |  Giá thuê: <span> 3.600.000₫</span> </div>
    </div>
    <div class="p-right">
    <div class="e-location"><a href="http://chothuetro.com/cho-thue-phong-tro/tp-ho-chi-minh" title="Khu vực TP Hồ Chí Minh">TP Hồ Chí Minh</a></div>
    <div class="e-public">29/04/2016</div>
    </div>
   </li>
    </ul>
            
                    </div>
</tiles:putAttribute>
</tiles:insertDefinition>