<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<tiles:insertDefinition name="tcareness">
<tiles:putAttribute name="body">
<link rel="stylesheet" href="resources/css/mainContent.css">

<div id="mainContent">
	
	<div id="Direction">

	<a href="#">Nước Hoa</a> <!--Hiển thị vị trí mình đang ở trong website-->

	</div>
	
	<div id="Filter">
		<div id="Woman">
			<h1>NỮ</h1>
			<ul>
				<li><a href="#">Nước Hoa</a></li>
				<li><a href="#">Dành Cho Du Lịch</a></li>
				<li><a href="#">GiftSet</a></li>
			</ul>
		</div>
		<div id="Man">
			<h1>NAM</h1>
			<ul>
				<li><a href="#">Nước Hoa</a></li>
				<li><a href="#">Dành Cho Du Lịch</a></li>
				<li><a href="#">GiftSet</a></li>
			</ul>
		</div>
	</div>
	
	<div id="Images">
	</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>